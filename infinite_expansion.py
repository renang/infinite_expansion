"""
This file contains the numerical analysis for calculating bounds on
the seed length of the unbounded randomness expansion protocol
described in a paper by Miller and Shi, found here:
http://arxiv.org/abs/1402.0489

The code supplements the analytic analysis in my paper "Bounding the
seed length of Miller and Shi's unbounded randomness protocol" (arXiv
not yet read). Essentially, every time a phrase of the type "can be
found by numerical calculations" appears in the paper, the
calculations are performed here.

This file was written by Renan Gross, free under GPL. If you find any
bugs, have comments, or want information, contact
renan.gross@gmail.com

A small overview:

************************************
run_protocol(eps, n)
************************************
The main workhorse used for analysis is "run_protocol(eps, n)". This
function runs protocol R just once. It returns the number of bits used
in the seed and the number of bits generated, such that the output is
(at least) n times as long as the input.  (We say at least, because it
may be that for the given epsilon value, the initial seed cannot be so
small as to allow only n. But this is ok!)

The parameter n has a default value (of 1.05).

Sample usage:
>>> eps = 2.5E-7
>>> run_protocol(eps, 1.2)
(728946.638113, 874134.959377)

In general this function can take a lot of time (an hour, even), since
it optimizes over values of delta, eta, q_0, k_0. You can cut down
running time (without losing a lot of accuracy) by cutting down grid
resolutions, and checking less delta and eta values.  (For example,
instead of taking 30 deltas, you can take just [0.5,0.6,0.7,0.8,0.9],
and from my experience these 5 will yield results that are only 1000's
of bits away).

************************************
minimize_number_of_bits_needed_for_cycle(eps)
************************************
This implements the scheme described at the end of the paper, of
finding an n value such that the first round of the protocol doesn't
waste any bits.  In the end it returns such an n, which you can put
into "run_protocol". It basically does binary search on the n value
used for a fixed amount of iterations. As this calls "run_protocol",
it can also take a lot of time to run.

Sample usage:
>>> eps = 2.5E-7
>>> n = minimize_number_of_bits_needed_for_cycle(eps)
>>> run_protocol(eps, n)


Most other functions are either simple helpers, or are of the form
"get_bits_with_X_constant", which just assume that some parameter is
fixed and optimize on the others.

"""
import math
import numpy
import scipy.optimize
import matplotlib.pyplot as plt
from matplotlib import cm
from mpl_toolkits.mplot3d import axes3d
from scipy import stats

# TODO: when extracting, is there a better ratio than the "m for 4k"?

cv = 0.14
K = math.sqrt(2)+1
N_PLAYERS = 3
QK_RESOLUTION = 20

def log2(x):
    return math.log(x,2)

    
def big_PI_func(x,y):
    if x == 0:
        return pi_func(x)
    if y == 0 or y == 1: return 1
    s = 1.0/(1+2*x)
    return 1 - ((1+2*x)/x)*log2( (1-y)**s + y**s)

    
def pi_func(x):
    if x == 0 or x == 1: return 1
    return 1 + 2*(x*log2(x) + (1-x)*log2(1-x))

    
def pi_deriv_func(x):
    return 2*log2(x) - 2*log2(1-x)

    
def log_arg_full(q,k,eta,t):
    """
    Returns the full argument to the log in the "max" expression.
    """
    r1 = -cv/pi_deriv_func(eta)
    return (1-q)*(2**(-r1*q*k*big_PI_func(r1*q*k,t)))+q*(1-t*(1-2**(-k))*(cv**(1+r1*q*k)))

    
def max_expression(q,k,eta):
    """
    For a given q,k, returns the max over all t of the full right hand side
    expression.
    """
    r1 = -cv/pi_deriv_func(eta)
    f = lambda t: (-1.0/(r1*k*q))*log2(log_arg_full(q,k,eta,t))    
    a = scipy.optimize.minimize_scalar(f, bounds=(0,1), method='Bounded')    
    return a.fun

    
def get_corollary_I2_possible_qks(delta, eta):
    """
    Returns all possible q0, k0 values promised by corollary I2.
    Of course, there is a continuum of them, so it samples a grid.
    Returns a three tuple (Z, QS, KS):
    - QS, KS are grids containing the q and k values.
    - Z is a grid that has non-zero entries only of the q0,k0 is good,
      meaning, the T function is bounded as we want it.
    
    All grids are of size QK_RESOLUTION ** 2.
    """
    r1 = -cv/pi_deriv_func(eta)
    compare_to = pi_func(eta) - delta/2.0 + eta/r1

    def comparator(q,k):
        a = max_expression(q,k,eta)
        return a if a>=compare_to else 0

    # Since we force q0 to be smaller than delta/2n, we can set that to be the
    # upper limit for q0.
    qs = numpy.linspace(0.0001, 0.95 * delta/(2*N_PLAYERS), QK_RESOLUTION)
    ks = numpy.linspace(0.001, 8, QK_RESOLUTION)
    QS,KS = numpy.meshgrid(qs, ks)
    Z = numpy.vectorize(comparator)(QS,KS)

    found_nonzero = False
    for i in xrange(QK_RESOLUTION):
        for j in xrange(QK_RESOLUTION):
            k = KS[i,j]
            q = QS[i,j]
            if Z[i,j] > 0:
                found_nonzero = True               

    if not found_nonzero:
        # There was no greater than 0 value, no solution
        # This is because our eta value was too large. Sucks to be us.
        raise ValueError("Eta value too large for finding q0,k0")

    return Z, QS, KS


def bits_needed_single_stage(q, N, eps, delta):
    """
    How many initial random bits are needed for running the protocol with
    the desired parameters.
    q - xor game frequency
    N - length of output
    eps - closeness to uniformity (for extractor)
    """
    t = log2(512 * (((1-delta)*3.0/4.0)**8) * (N**9) / (eps**8)) 
    d = t*(t+1) / math.log(2)
    gs = 2*N*(-q*log2(q) - (1-q)*log2(1-q))
    bits_needed = gs + d
    return bits_needed
    

def extract_bits_from_N(N, delta):
    """
    Apply our extractor to N bits with (1-delta)N bits of min-entropy.
    """
    return N * 0.25 * (1-delta)


def find_q_N_for_n_times_as_many(epsilon, delta, n, b, q0, threshold=0.001):
    """
    Returns the N and q needed in order to generate n times as many
    bits as you need to start generate.
    """
    # This makes sure that q0 is small enough to fit the theory (q0<delta/2n)
    q0 = min(q0, delta/(2*N_PLAYERS))    
    qn = log2(K/eps)/b
    min_N = qn/q0

    # The plan: we basically do a binary search: we first find an N large
    # enough so that you generate too many bits, then home in on the actual
    # value until a threshold is reached.

    bits_needed = bits_needed_single_stage(q0, min_N, epsilon, 4*delta)
    bits_generated = extract_bits_from_N(min_N, 4*delta) # *4 b.c we need the delta for eta, which is 4 times as large.
    
    # If you are already there with the minimum N allowed, go right ahead
    # and return.
    if bits_generated >= n * bits_needed:
        return (q0, min_N)

    # First, keep multiplying N by two until you have reached your mark.
    bits_needed = 0
    bits_generated = 0
    N = min_N / 2.0
    i = 0
    while bits_generated <= n * bits_needed:
        previous_N = N
        N = N * 2.0
        q = qn / N    # Can only make q smaller
        bits_needed = bits_needed_single_stage(q, N, epsilon, 4*delta)
        bits_generated = extract_bits_from_N(N, 4*delta) # *4 b.c we need the delta for eta, which is 4 times as large.

    # Of course, you may have overshot by a factor of two!
    # Binary search the rest until the specified threshold is reached.
    low = previous_N
    high = N
    while abs(bits_generated - n*bits_needed) > threshold * bits_needed:
        N = (high + low) / 2.0
        q = qn / N
        bits_needed = bits_needed_single_stage(q, N, epsilon, 4*delta)
        bits_generated = extract_bits_from_N(N, 4*delta) # *4 b.c we need the dםנelta for eta, which is 4 times as large.
        if bits_generated > n*bits_needed:
            high = N
        else:
            low = N
    return (q, N)

    
def find_eta_upper_bound(delta):
    """
    For a given delta, any eta chosen must be smaller than thixs one, in
    order to ensure that the pi func will be delta-close to 1. However,
    it does not ensure that the maxT equation will be satisfied - you might
    need smaller etas than that.
    """
    # Remember: We need to find an x such that
    # -xlogx -(1-x)log(1-x) <= delta/4
    f = lambda x: delta/2.0 - 1 + pi_func(x)    
    x0 = scipy.optimize.brentq(f, 0, 0.5)
    return x0 * cv
    

def is_valid_eta(delta, eta):
    """
    Returns wether a given eta will yield possible q,k values or not.
    (It may be, probably because of the inequalities made in the bound
    analysis, that there is no solution for a given eta).
    """
    try:
        get_corollary_I2_possible_qks(delta, eta)
        return True
    except ValueError, e:
        return False

        
def get_bits_for_fixed_parameters(eps, delta, n, eta, q0, k0):
    """
    Having fixed all the parameters (except for q), this function finds a q
    and N such that you will get n times as much output as input, and returns
    a tuple (bits_needed, bits_generated).
    """
    b = min(-k0 * delta * cv / (4*pi_deriv_func(eta)),
            0.5*log2(math.e)*((delta/(2*N_PLAYERS) - q0)**2)/q0)
    (q, N) = find_q_N_for_n_times_as_many(eps, delta, n, b, q0)
    bits_needed = bits_needed_single_stage(q, N, eps, 4*delta)
    bits_generated = extract_bits_from_N(N, 4*delta) # *4 b.c we need the delta for eta, which is 4 times as large.
    return bits_needed, bits_generated

    
def get_bits_needed_for_fixed_eta(eps, delta, n, eta):
    """
    Go over all possible q0, k0 values for a given eta and delta,
    yielding the amount of bits needed to multiply by n.
    Throws if eta is too large.
    """
    Z, QS, KS = get_corollary_I2_possible_qks(delta, eta)
    best_bits_needed = 0
    best_bits_generated = 0
    rows, cols = Z.shape
    best_k = 0
    for i in xrange(rows):
        for j in xrange(cols):
            if Z[i,j] !=0:
                q0 = QS[i,j]
                k0 = KS[i,j]
                bits_needed, bits_generated = get_bits_for_fixed_parameters(eps, delta, n, eta, q0, k0)
                
                if best_bits_needed == 0:
                    best_bits_needed = bits_needed
                    best_bits_generated = bits_generated
                    best_k = k0
                else:
                    if bits_needed < best_bits_needed:
                        best_bits_needed = bits_needed
                        best_bits_generated = bits_generated
                        best_k = k0

    return best_bits_needed, best_bits_generated


def get_bits_needed_for_fixed_delta(eps, delta_for_eta, n=1.05):
    """
    Delta is the "delta_for_eta", meaning, the distance from 1 of the total
    min-entropy after playing protocol R.
    """
    eta_upper_bound =  find_eta_upper_bound(delta_for_eta)
    delta_for_corollary = delta_for_eta / 4.0
    delta = delta_for_corollary

    # Finding good eta:
    high = eta_upper_bound
    low = 0
    ETA_ITERATION_NUMBER = 20
    count = 0
    while count < ETA_ITERATION_NUMBER:
        eta = (high + low) / 2
        if is_valid_eta(delta, eta):
            # eta is ok; you can raise it a bit higher.
            low = eta
        else:
            # Eta too high, reduce it.
            high = eta            
        count+=1                
    # So you won't throw: (this will fail only if you were high
    # all those times, which is highly improbable)
    eta_upper_bound = low
    eta_lower_bound = eta_upper_bound*(1E-12)

    # Note: when using numpy's optimize, it appears to hit a local minimum,
    # which is not global. It might be that there shouldn't be such a minimum,
    # but in any case I leave the self defined optimization here.
    ETA_RESOLUTION = 40
    etas = numpy.linspace(eta_lower_bound, eta_upper_bound, ETA_RESOLUTION)
    best_bits_needed = 0
    best_bits_generated = 0
    for eta in etas:
        try:
            bits_needed, bits_generated = get_bits_needed_for_fixed_eta(eps, delta, n, eta)
            if best_bits_needed == 0:
                best_bits_needed = bits_needed
                best_bits_generated = bits_generated
            else:
                if bits_needed < best_bits_needed:
                    best_bits_needed = bits_needed
                    best_bits_generated = bits_generated
        except ValueError, e:
            print "This eta didn't make it...", eta
            continue
    if best_bits_needed == 0:
        raise ValueError("No eta made it! Didn't find anything for this delta")
    return best_bits_needed, best_bits_generated    
    

def run_protocol(eps, n=1.05):
    """
    Runs protocol R just once (exponential expansion with one device),
    yielding n times as many bits as you put in.
    Returns a tuple (bits_needed, bits_generated). 
    """
    deltas = numpy.linspace(0.05, 0.995, 30)
    # Uncomment the next lineif you want to speed up your calculations by
    # a factor of 6,  without losing a lot of accuracy (from my experience
    # so far). 
    #deltas = [0.5, 0.6, 0.7, 0.8, 0.9]
    best_bits_needed = 0
    best_bits_generated = 0
    for delta_for_eta in deltas:
        bits_needed, bits_generated = get_bits_needed_for_fixed_delta(eps, delta_for_eta, n)
        if best_bits_needed == 0:
            best_bits_needed = bits_needed
            best_bits_generated = bits_generated
        else:
            if bits_needed < best_bits_needed:
                best_bits_needed = bits_needed
                best_bits_generated = bits_generated
    
    return best_bits_needed, best_bits_generated    
    

def minimize_number_of_bits_needed_for_cycle(eps):
    """
    Calculate the n value needed to produce just enough bits so that the next
    level (with a smaller epsilon) can be produced from your output.
    You only need to calculate this for the first stage, since the increase
    in the number of bits gets smaller for increasingly smaller epsilons.
    """
    high = 2.0
    low = 1.0
    n = high
    ITERATION_NUMBER = 6
    for i in xrange(ITERATION_NUMBER):
        n = (high+low)/2.0
        bits_needed1, bits_generated1 = run_protocol(eps, n)
        bits_needed2, bits_generated2 = run_protocol(eps*0.5, n)

        if bits_generated1 < bits_needed2:
            # Not enough bits were generated now
            low = n
        else:
            # Too many bits were generated, we are wasting initial randomness
            high = n
    return n


if __name__ == "__main__":
    # The following are some specific calculations for specific constants;
    # They are not a necessary or generic part of the analysis and program.
    epsilons = [0.25 * 10**(-i) for i in xrange(1,12)]
    #regular_results = [run_protocol(eps, minimize_number_of_bits_needed_for_cycle(eps)) for eps in epsilons]
    #without_results = [run_protocol(eps, minimize_number_of_bits_needed_for_cycle(eps)) for eps in epsilons]
    x = numpy.array([log2(1.0/(4*eps)) for eps in epsilons])
    with_extractor = [r[0] for r in regular_results]
    without_extractor = [r[0] for r in without_results]
    plt.plot(x, with_extractor, "b.")
    plt.plot(x, without_extractor, "r.")
    slope, intercept, r_value, p_value, std_err = stats.linregress(x, with_extractor)
    plt.plot(x, x*slope + intercept, "blue")
    slope, intercept, r_value, p_value, std_err = stats.linregress(x, without_extractor)
    plt.plot(x, x*slope + intercept, "red")
    plt.ylim(0)
    plt.legend(["With extractor", "Without extractor"],"northwest")
    plt.xlabel(r"$\log_{2}(1/\varepsilon$)")
    plt.ylabel("Bits needed")
    plt.title(r"Bits needed vs. $\log_{2}(1/\varepsilon$)")
    plt.show()